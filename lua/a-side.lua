local mod = XalumMods.Leah

mod.RadialBurstByWeaponType = {
	[WeaponType.WEAPON_TEARS] = function(player)
		local doTech2 = player:HasCollectible(CollectibleType.COLLECTIBLE_TECHNOLOGY_2)

		for i = 1, 12 do
			if doTech2 and i % 2 == 1 then
				player:FireTechLaser(player.Position, LaserOffset.LASER_BRIMSTONE_OFFSET, Vector(1, 0):Rotated(i * 360 / 12), false, false, player, 0.5)
			else
				local tear = player:FireTear(player.Position, Vector(player.ShotSpeed * 10, 0):Rotated(i * 360 / 12), true, true, true, player, 0.5)
				if tear.Variant == TearVariant.BLUE then
					tear:ChangeVariant(TearVariant.BLOOD)
				end
			end
		end
	end,

	[WeaponType.WEAPON_BRIMSTONE] = function(player)
		for i = 1, 8 do
			local laser = player:FireBrimstone(Vector(1, 0):Rotated(i * 360 / 8), player, 0.5)
		end
	end,

	[WeaponType.WEAPON_LASER] = function(player)
		for i = 1, 12 do
			player:FireTechLaser(player.Position, LaserOffset.LASER_BRIMSTONE_OFFSET, Vector(1, 0):Rotated(i * 360 / 12), false, false, player, 0.5)
		end
	end,

	[WeaponType.WEAPON_KNIFE] = function(player)
		for i = 1, 12 do
			local knife = player:FireKnife(player, i * 360 / 12)
			knife:Shoot(1, player.TearRange)
			mod.GetData(knife).isRadialKnife = true
		end
	end,

	[WeaponType.WEAPON_BOMBS] = function(player)
		for i = 1, 8 do
			player:FireBomb(player.Position, Vector(player.ShotSpeed * 10, 0):Rotated(i * 360 / 8), player)
		end
	end,

	--[[[WeaponType.WEAPON_ROCKETS] = function(player) -- Not currently working
		for i = 1, 8 do
			player:UseActiveItem(CollectibleType.COLLECTIBLE_DOCTORS_REMOTE, UseFlag.USE_NOANIM)
			player:StopExtraAnimation()
		end

		local i = 1
		for _, target in pairs(Isaac.FindByType(1000, 30, -1)) do
			print("pre")
			if target.Position:Distance(player) < 1 then
				print("found")
				target.Friction = 0.8
				target.Velocity = Vector(player.ShotSpeed * 10, 0):Rotated(i * 360 / 8)
				i = i + 1
			end
		end
	end,]]

	[WeaponType.WEAPON_TECH_X] = function(player)
		local laser = player:FireTechXLaser(player.Position, Vector.Zero, player.TearRange, player, 0.5)
		laser.Timeout = 30
	end,
}

function mod.DoesSodomGomorrahCollideGrid(grid)
	local typ = grid and grid:GetType()
	return  grid and (
		grid.CollisionClass >= GridCollisionClass.COLLISION_WALL or
		(typ == GridEntityType.GRID_LOCK and grid.State == 0) or
		typ == GridEntityType.GRID_PILLAR
	)
end

function mod.CheckGhostGridCollision(player, ghost)
	local room = game:GetRoom()
	local x = ghost.Velocity.X
	local y = ghost.Velocity.Y

	local check = ghost.Position + Vector(x, 0):Resized(math.abs(x) + player.Size)
	local grid = room:GetGridEntityFromPos(check)

	if grid and mod.DoesSodomGomorrahCollideGrid(grid) then
		local distance = 20 - math.abs(check.X - grid.Position.X)
		x = Vector(x, 0):Resized(math.abs(x) - distance).X
	end

	check = ghost.Position + Vector(0, y):Resized(math.abs(x) + player.Size)
	grid = room:GetGridEntityFromPos(check)

	if grid and mod.DoesSodomGomorrahCollideGrid(grid) then
		local distance = 20 - math.abs(check.Y - grid.Position.Y)
		y = Vector(y, 0):Resized(math.abs(y) - distance).Y
	end

	ghost.Velocity = Vector(x, y)
end

function mod.CanLeahSummonBloodGhost(player)
	local playerEffects = player:GetEffects()

	return (
		not player:IsHoldingItem() and
		not playerEffects:HasCollectibleEffect(CollectibleType.COLLECTIBLE_NOTCHED_AXE) and
		not playerEffects:HasCollectibleEffect(CollectibleType.COLLECTIBLE_URN_OF_SOULS)
	)
end

function mod.IsLeahTryingToSpawnBloodGhost(player)
	return (
		player:GetShootingInput():Length() > 0 or
		Input.IsMouseBtnPressed(0)
	)
end

mod:AddCallback(ModCallbacks.MC_EVALUATE_CACHE, function(_, player)
	if player:GetPlayerType() == mod.PLAYER_TYPE.LEAH then
		player.CanFly = true
	end
end, CacheFlag.CACHE_FLYING)

mod:AddCallback(ModCallbacks.MC_EVALUATE_CACHE, function(_, player)
	if player:GetPlayerType() == mod.PLAYER_TYPE.LEAH then
		player.TearRange = player.TearRange - 140
	end
end, CacheFlag.CACHE_RANGE)

mod:AddCallback(ModCallbacks.MC_POST_PLAYER_INIT, function(_, player)
	if player:GetPlayerType() == mod.PLAYER_TYPE.LEAH then
		player:AddCostume(Isaac.GetItemConfig():GetCollectible(CollectibleType.COLLECTIBLE_VENUS), false)
	end
end)

mod:AddCallback(ModCallbacks.MC_POST_PEFFECT_UPDATE, function(_, player)
	local data = mod.GetData(player)
	local movement = data.cachedMovementInput or player:GetMovementInput():Normalized()

	if data.bloodghost then
		if Input.IsMouseBtnPressed(0) then
			local targetVelocity = Input.GetMousePosition(true) - data.bloodghost.Position
			targetVelocity:Resize(math.min(targetVelocity:Length(), player.MoveSpeed * 25))

			data.bloodghost.Velocity = mod.Lerp(data.bloodghost.Velocity, targetVelocity, 0.15)
		else
			data.bloodghost:AddVelocity(movement * player.MoveSpeed * 1.75)
			data.bloodghost.Velocity = data.bloodghost.Velocity * 0.9
		end

		mod.CheckGhostGridCollision(player, data.bloodghost)

		local differenceVector = (data.bloodghost.Position - player.Position)

		for i = 1, differenceVector:Length() / 60 do
			local spawnPosition = player.Position + differenceVector:Resized(math.random() * differenceVector:Length())
			local trailEffect = Isaac.Spawn(1000, 111, 0, spawnPosition, RandomVector() * 3, player)
			trailEffect.DepthOffset = -100000
			trailEffect.SpriteOffset = Vector(0, -16)

			trailEffect:GetSprite():SetFrame(math.random(2, 3))
		end

		if data.gutsTears then
			if data.bloodghost.FrameCount % 5 == 0 then
				local gutsTear = player:FireTear(player.Position, Vector.Zero, false, true, false, player, 1)
				gutsTear:AddTearFlags(TearFlags.TEAR_PIERCING)
				gutsTear:AddTearFlags(TearFlags.TEAR_SPECTRAL)

				gutsTear.KnockbackMultiplier = 0.1

				gutsTear.Visible = false
				gutsTear.FallingAcceleration = -0.1
				gutsTear.FallingSpeed = 0

				gutsTear:GetData().leahGutsTear = true
				sfx:Stop(SoundEffect.SOUND_TEARS_FIRE)

				table.insert(data.gutsTears, gutsTear)
			end

			for i = #data.gutsTears, 1, -1 do
				local tear = data.gutsTears[i]

				if tear.Position:Distance(data.bloodghost.Position) < 20 then
					tear:Remove()
					table.remove(data.gutsTears, i)
				else
					local targetVelocity = (data.bloodghost.Position - tear.Position):Resized(20)
					local playerDistance = tear.Position:Distance(player.Position)

					tear.Position = player.Position + (data.bloodghost.Position - player.Position):Resized(playerDistance)
					tear.Velocity = mod.Lerp(tear.Velocity, targetVelocity, 0.75)
				end
			end
		end
	end

	if mod.IsLeahTryingToSpawnBloodGhost(player) then
		if mod.CanLeahSummonBloodGhost(player) then
			if not data.bloodghost then
				local mouseAngle = (Input.GetMousePosition(true) - player.Position):GetAngleDegrees()
				local launchVector = Input.IsMouseBtnPressed(0) and Vector.FromAngle(mouseAngle):Normalized() or movement

				local bloodGhost = Isaac.Spawn(1000, mod.EFFECTS.LEAH_BLOODGHOST, 0, player.Position, launchVector * 15, player):ToEffect()
				data.bloodghost = bloodGhost

				sfx:Play(SoundEffect.SOUND_MEAT_JUMPS)
			end

			if not data.evisGuts or not data.evisGuts:Exists() then
				local guts = Isaac.Spawn(865, 10, 0, player.Position + Vector(0, 1), Vector.Zero, player):ToNPC()
				guts:AddEntityFlags(EntityFlag.FLAG_FRIENDLY)
				guts.Parent = player
				guts.Target = data.bloodghost
				guts.DepthOffset = -1000

				local gutsSprite = guts:GetSprite()
				gutsSprite:Load("gfx/leahs_guts.anm2", true)
				gutsSprite:Play("Gut")

				data.evisGuts 	= guts
				data.gutsTears	= {}
			end
		end
	elseif data.bloodghost then
		local doRadialBurst = player.Position:Distance(data.bloodghost.Position) > 80

		if data.bloodghost.FrameCount > 3 then
			player.Position = data.bloodghost.Position
			Isaac.Spawn(1000, 16, 3, player.Position, Vector.Zero, player)
			Isaac.Spawn(1000, 16, 4, player.Position, Vector.Zero, player)

			sfx:Play(SoundEffect.SOUND_MEATY_DEATHS)
		end

		if doRadialBurst then
			local weaponFunctionFound
			for weapon, func in pairs(mod.RadialBurstByWeaponType) do
				if player:HasWeaponType(weapon) then
					weaponFunctionFound = true
					func(player)
					break
				end
			end 

			if not weaponFunctionFound then
				mod.RadialBurstByWeaponType[WeaponType.WEAPON_TEARS](player)
			end
		end

		data.bloodghost:Remove()
		data.evisGuts:Remove()

		for _, tear in pairs(data.gutsTears) do
			tear:Remove()
		end

		data.bloodghost = nil
		data.evisGuts 	= nil
		data.gutsTears 	= nil
	end
end, mod.PLAYER_TYPE.LEAH)

mod:AddCallback(ModCallbacks.MC_POST_NEW_ROOM, function()
	for _, player in pairs(Isaac.FindByType(1, 0, mod.PLAYER_TYPE.LEAH)) do
		local data = mod.GetData(player)

		if data.bloodghost then
			if data.bloodghost:Exists() then data.bloodghost:Remove() end
			data.bloodghost = nil
		end

		if data.evisGuts then
			if data.evisGuts:Exists() then data.evisGuts:Remove() end
			data.evisGuts = nil
		end

		if data.gutsTears then
			for _, tear in pairs(data.gutsTears) do
				if tear:Exists() then tear:Remove() end
			end
			data.gutsTears = nil
		end
	end
end)

mod:AddCallback(ModCallbacks.MC_NPC_UPDATE, function(_, npc)
	if npc.Variant == 10 and npc.SpawnerEntity and npc.SpawnerEntity:ToPlayer() then
		local sprite = npc:GetSprite()
		if sprite:GetFilename() == "gfx/leahs_guts.anm2" and npc.FrameCount % 3 == 0 then
			sprite:ReplaceSpritesheet(0, "gfx/effects/effect_leahs_guts_" .. math.floor(1 + (npc.FrameCount / 3) % 4) .. ".png")
			sprite:LoadGraphics()
		end
	end
end, 865)

mod:AddCallback(ModCallbacks.MC_POST_EFFECT_INIT, function(_, effect)
	effect:GetSprite():SetOverlayFrame("HeadDown", 0)
end, mod.EFFECTS.LEAH_BLOODGHOST)

mod:AddCallback(ModCallbacks.MC_POST_KNIFE_UPDATE, function(_, knife)
	if mod.GetData(knife).isRadialKnife and not knife:IsFlying() then
		knife:Remove()
	end
end)

for hook = InputHook.IS_ACTION_PRESSED, InputHook.IS_ACTION_TRIGGERED do
	mod:AddCallback(ModCallbacks.MC_INPUT_ACTION, function(_, entity, _, action)
		if entity then
			local player = entity:ToPlayer()
			if player and player:GetPlayerType() == mod.PLAYER_TYPE.LEAH and mod.CanLeahSummonBloodGhost(player) then
				if action >= ButtonAction.ACTION_SHOOTLEFT and action <= ButtonAction.ACTION_SHOOTDOWN then
					return false
				end
			end
		end
	end, hook)
end

mod:AddCallback(ModCallbacks.MC_INPUT_ACTION, function(_, entity, hook, action)
	if entity and not mod.AntiMoveCheckRecurse then
		local player = entity:ToPlayer()
		if player and player:GetPlayerType() == mod.PLAYER_TYPE.LEAH then
			if action <= ButtonAction.ACTION_DOWN then
				if mod.IsLeahTryingToSpawnBloodGhost(player) and mod.CanLeahSummonBloodGhost(player) then
					mod.AntiMoveCheckRecurse = true
					mod.GetData(player).cachedMovementInput = player:GetMovementInput():Normalized()
					mod.AntiMoveCheckRecurse = false

					return 0
				else
					mod.GetData(player).cachedMovementInput = nil
				end
			end
		end
	end
end, InputHook.GET_ACTION_VALUE)