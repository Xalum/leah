local mod = RegisterMod("Leah", 1)
local version = "0.2.0"

game = Game()
sfx = SFXManager()

XalumMods = XalumMods or {}
XalumMods.Leah = mod

mod.PLAYER_TYPE = {
	LEAH 	= Isaac.GetPlayerTypeByName("Leah"),
	LEAH_B	= Isaac.GetPlayerTypeByName("Leah", true),
}

mod.EFFECTS = {
	LEAH_BLOODGHOST	= Isaac.GetEntityVariantByName("Leah's Bloodghost")
}

function mod.GetData(entity)
	local data = entity:GetData()
	data.xalumModsLeah = data.xalumModsLeah or {}
	return data.xalumModsLeah
end

function mod.Lerp(initial, target, percentage)
	return initial + (target - initial) * percentage
end

include("lua.a-side")
include("lua.b-side")

print("Leah v" .. version .. " Loaded Successfully")